﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Ridder.Client.SDK;
using Ridder.Client.SDK.Extensions;

namespace SampleAntonRechtenproject
{
    public class Program
    {
        //private static string _sqlDataSource = @"LT115\RIDDERIQ"; //Lokaal testen
        private static string _sqlDataSource = @"SRV-APP03\RIDDERIQ"; //Live bij Anton

        static void Main(string[] args)
        {
            var mainSession = new SdkSession();
            Console.WriteLine("Inloggen in Anton Groep.");
            var mainRoles = GetRoles(mainSession);
            var mainRolePermissions = GetRolePermissions(mainSession).ToList();

            string fileCompanies = $"{AppDomain.CurrentDomain.BaseDirectory}/Companies.txt";

            if (!File.Exists(fileCompanies))
            {
                Console.WriteLine("Tekst-bestand met ingestelde bedrijven niet gevonden.");
                Console.ReadKey();
                return;
            }

            var companies = File.ReadAllLines(fileCompanies);

            if (!companies.Any())
            {
                return;
            }

            string fileComponentes = $"{AppDomain.CurrentDomain.BaseDirectory}/Components.txt";

            if (!File.Exists(fileComponentes))
            {
                Console.WriteLine("Tekst-bestand met ingestelde onderdelen niet gevonden.");
                Console.ReadKey();
                return;
            }

            var components = File.ReadAllLines(fileComponentes);

            if (!components.Any())
            {
                return;
            }

            if(components.Contains("Filters") || components.Contains("Layouts"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Filters en layouts exporteren niet meer beschikbaar. Gebruik de nieuwe Customizing exporter.");
                Console.ResetColor();
                Console.ReadKey();
                return;
            }


            foreach (var company in companies)
            {
                var configuration = new SdkConfiguration()
                {
                    CompanyName = company,
                    UserName = mainSession.Configuration.UserName,
                    Password = mainSession.Configuration.Password,
                    PersistSession = true,
                    RidderIQClientPath = mainSession.Configuration.RidderIQClientPath,
                };

                var sessionCompany = new SdkSession(configuration);
                try
                {
                    sessionCompany.Login();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Ingelogd in {company}");
                    Console.ResetColor();
                }
                catch
                {
                    Console.WriteLine($"Het is niet gelukt om in te loggen in bedrijf {company}, controleer het Administrator-wachtwoord.");
                    continue;
                }

                //Vergelijk rollen
                var rolesCompany = CompareRolesCompany(sessionCompany, mainRoles, company, mainSession);

                var refreshUsers = false;

                if (components.Contains("Rechten"))
                {
                    Console.WriteLine($"Start export rechten {company}.");

                    if (ProcessRolePermisions(sessionCompany, mainRoles, mainRolePermissions, rolesCompany, company,
                        mainSession))
                    {
                        refreshUsers = true;
                    }

                    Console.WriteLine($"Export rechten {company} afgerond.");
                }

                /*
                if (components.Contains("Filters"))
                {
                    Console.WriteLine($"Start export filters {company}.");

                    ProcessFilters(mainSession, mainRoles, company, sessionCompany, rolesCompany);

                    Console.WriteLine($"Export filters {company} afgerond.");
                }
                
                
                if (components.Contains("Layouts"))
                {
                    Console.WriteLine($"Start export layouts {company}.");

                    ProcessLayouts(mainSession, mainRoles, company, sessionCompany, rolesCompany);

                    Console.WriteLine($"Export layouts {company} afgerond.");
                }*/

                if (components.Contains("Mailsjablonen"))
                {
                    Console.WriteLine($"Start export mailsjablonen {company}.");

                    ProcessMailSjablonen(mainSession, sessionCompany);

                    Console.WriteLine($"Export mailsjablonen {company} afgerond.");
                }

                if (refreshUsers)
                {
                    CreateNewRoleAndUserToRefreshPermissions(sessionCompany);
                }

                sessionCompany.Logout();
                Console.WriteLine($"Export onderdelen {company} afgerond.");
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Export onderdelen afgerond voor alle bedrijven.");
            Console.ResetColor();
            Console.ReadKey();
        }

        private static void ProcessMailSjablonen(SdkSession mainSession, SdkSession sessionCompany)
        {
            var mailReportSettingsMainCompany = GetMailReportSettings(mainSession);
            var mailReportSettingsSessionCompany = GetMailReportSettings(sessionCompany);

            List<MailReportSettingToChange> mailReportSettingsToChange = new List<MailReportSettingToChange>();

            //Aanmaken en verwijderen doen we niet. Dit kunnen we pas inrichten indien de mailrapport-id omgezet is naar een Guid
            foreach (var mailReportSettingSessionCompany in mailReportSettingsSessionCompany)
            {
                var sameMailReportSettingMainSession =
                    mailReportSettingsMainCompany.FirstOrDefault(x =>
                        x.SameMailReportSetting(mailReportSettingSessionCompany));

                if (sameMailReportSettingMainSession == null)
                {
                    continue;
                }

                if (mailReportSettingSessionCompany.PropertiesMailReportSettingAreEqual(
                    sameMailReportSettingMainSession))
                {
                    continue; //Eigenschappen zijn gelijk
                }

                mailReportSettingsToChange.Add(new MailReportSettingToChange()
                {
                    MainMailReportSetting = sameMailReportSettingMainSession,
                    ChildMailReportSetting = mailReportSettingSessionCompany,
                });
            }

            if (!mailReportSettingsToChange.Any())
            {
                Console.WriteLine("Geen mailsjablonen die bijgewerkt moeten worden.");
                return;
            }

            var rsMailReportSettings = sessionCompany.GetRecordsetColumns("R_MAILREPORTSETTING", "",
                $"PK_R_MAILREPORTSETTING IN ({string.Join(",", mailReportSettingsToChange.Select(x => x.ChildMailReportSetting.MailReportId))})");
            rsMailReportSettings.UpdateWhenMoveRecord = false;
            rsMailReportSettings.UseDataChanges = false;

            rsMailReportSettings.MoveFirst();

            while (!rsMailReportSettings.EOF)
            {
                var mainMailReportSetting = mailReportSettingsToChange.First(x =>
                    x.ChildMailReportSetting.MailReportId ==
                    (int)rsMailReportSettings.Fields["PK_R_MAILREPORTSETTING"].Value).MainMailReportSetting;

                rsMailReportSettings.Fields["DESCRIPTION"].Value = mainMailReportSetting.Description;
                rsMailReportSettings.Fields["FILENAME"].Value = mainMailReportSetting.FileName;

                rsMailReportSettings.Fields["FK_USERREPORT"].Value = (mainMailReportSetting.UserReport.HasValue)
                    ? mainMailReportSetting.UserReport
                    : (object)DBNull.Value;

                rsMailReportSettings.Fields["FK_SYSTEMREPORT"].Value = (mainMailReportSetting.SystemReport.HasValue)
                    ? mainMailReportSetting.SystemReport
                    : (object)DBNull.Value;

                rsMailReportSettings.Fields["FROM"].Value = mainMailReportSetting.From;
                rsMailReportSettings.Fields["TO"].Value = mainMailReportSetting.To;
                rsMailReportSettings.Fields["CC"].Value = mainMailReportSetting.CC;
                rsMailReportSettings.Fields["BCC"].Value = mainMailReportSetting.BCC;
                rsMailReportSettings.Fields["SUBJECT"].Value = mainMailReportSetting.Subject;
                rsMailReportSettings.Fields["SALUTATION"].Value = mainMailReportSetting.Salutation;
                rsMailReportSettings.Fields["BODY"].Value = mainMailReportSetting.HtmlBody;
                rsMailReportSettings.Fields["SCRIPT"].Value = mainMailReportSetting.Script;

                foreach (var mainTranslation in mainMailReportSetting.Translations)
                {
                    sessionCompany.Sdk.SaveTranslatableFieldValue("R_MAILREPORTSETTING", "BODY",
                        mainTranslation.Key, (int)rsMailReportSettings.Fields["PK_R_MAILREPORTSETTING"].Value,
                        mainTranslation.Value);
                }

                rsMailReportSettings.MoveNext();
            }

            rsMailReportSettings.MoveFirst();

            try
            {
                var updateResult = rsMailReportSettings.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    Console.WriteLine(
                        $"Het bijwerken van mailsjablonen is mislukt; oorzaak {updateResult.First(x => x.HasError).GetResult()}");
                }
                else
                {
                    Console.WriteLine(
                        $"{rsMailReportSettings.RecordCount} mailsjablonen bijgewerkt. ({string.Join(",", mailReportSettingsToChange.Select(x => x.MainMailReportSetting.Description))})");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(
                    $"Het bijwerken van mailsjablonen is mislukt; oorzaak {e}");
            }
        }

        private static void ProcessLayouts(SdkSession mainSession, List<Role> mainRoles, string company,
            SdkSession sessionCompany, List<Role> rolesCompany)
        {
            var layoutsMainCompany = GetLayouts(mainSession);
            var layoutsSessionCompany = GetLayouts(sessionCompany);

            ResultCompareLayouts resultCompareLayouts = CompareLayouts(mainRoles, layoutsMainCompany,
                rolesCompany, layoutsSessionCompany, company);

            Console.WriteLine(
                $"To change:{resultCompareLayouts.LayoutsToChange.Count}; To create: {resultCompareLayouts.LayoutsToCreate.Count}; Not found in parent: {resultCompareLayouts.LayoutsNotFoundInParent.Count}");

            if (resultCompareLayouts.LayoutsToChange.Any())
            {
                ChangeLayouts(sessionCompany, resultCompareLayouts.LayoutsToChange);
            }

            if (resultCompareLayouts.LayoutsToCreate.Any())
            {
                CreateLayouts(sessionCompany, resultCompareLayouts.LayoutsToCreate,
                    mainRoles, rolesCompany);
            }

            if (resultCompareLayouts.LayoutsNotFoundInParent.Any())
            {
                DeleteLayouts(sessionCompany, resultCompareLayouts.LayoutsNotFoundInParent);
            }
        }

        private static List<Layout> GetLayouts(SdkSession sessionCompany)
        {
            var roles = GetRoles(sessionCompany);

            //1 = DesignTarget.System
            //Neem de agenda layouts niet mee, omdat deze in deze layouts de werknemers opgeslagen zijn in de layout
            return sessionCompany.GetRecordsetColumns("R_FORMSETTING",
                    "PK_R_FORMSETTING, LAYOUTNAME, DESIGNTARGET, NAME, FORMTYPE, CONTEXT, FK_ROLE, FORMLAYOUTINFO, FORMSETTINGINFO",
                    "DESIGNTARGET <> 'System' AND NAME <> 'Agenda' AND NAME <> 'Service planning'")
                .DataTable.AsEnumerable().Select(x => new Layout()
                {
                    LayoutId = x.Field<int>("PK_R_FORMSETTING"),
                    LayoutName = x.Field<string>("LAYOUTNAME"),
                    DesignTarget = x.Field<string>("DESIGNTARGET"),
                    Name = x.Field<string>("NAME"),
                    FormType = x.Field<string>("FORMTYPE"),
                    Context = x.Field<string>("CONTEXT"),
                    Role = (!x.Field<int?>("FK_ROLE").HasValue)
                        ? null
                        : roles.First(y => y.RoleId == x.Field<int>("FK_ROLE")),
                    FormLayoutInfo = x.Field<Byte[]>("FORMLAYOUTINFO"),
                    FormSettingInfo = x.Field<Byte[]>("FORMSETTINGINFO")
                }).Where(x => x.Name != "IQHomeWidgetForm").ToList();
        }

        private static void ProcessFilters(SdkSession mainSession, List<Role> mainRoles, string company, 
            SdkSession sessionCompany, List<Role> rolesCompany)
        {
            var filtersMainCompany = GetFilters(mainSession); 
            var filtersSessionCompany = GetFilters(sessionCompany);

            ResultCompareFilters resultCompareFilters = CompareFilters(mainRoles, filtersMainCompany,
                rolesCompany, filtersSessionCompany, company);

            Console.WriteLine(
                $"To change:{resultCompareFilters.FiltersToChange.Count}; To create: {resultCompareFilters.FiltersToCreate.Count}; Not found in parent: {resultCompareFilters.FiltersNotFoundInParent.Count}");


            if (resultCompareFilters.FiltersToChange.Any())
            {
                ChangeFilters(sessionCompany, resultCompareFilters.FiltersToChange);
            }

            if (resultCompareFilters.FiltersToCreate.Any())
            {
                CreateFilters(sessionCompany, resultCompareFilters.FiltersToCreate,
                    mainRoles, rolesCompany);
            }
            
            if (resultCompareFilters.FiltersNotFoundInParent.Any())
            {
                DeleteFilters(sessionCompany, resultCompareFilters.FiltersNotFoundInParent);
            }
        }

        private static void DeleteFilters(SdkSession sessionCompany, List<Filter> filtersNotFoundInParent)
        {
            foreach (var batch in filtersNotFoundInParent.Batch(2000))
            {
                // batch is een array van modellen 'RolePermissionToChange'.
                var rsFilters = sessionCompany.GetRecordsetColumns("R_TABLEQUERY", "",
                    $"PK_R_TABLEQUERY IN ('{string.Join("','", batch.Select(x => x.FilterId))}')");
                rsFilters.UpdateWhenMoveRecord = false;
                rsFilters.UseDataChanges = false;

                rsFilters.MoveFirst();
                while (!rsFilters.EOF)
                {
                    rsFilters.Delete();
                    rsFilters.MoveNext();
                }

                rsFilters.MoveFirst();

                try
                {
                    rsFilters.Update2();
                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        $"Het verwijderen van rechten is mislukt; oorzaak {e}");
                }
            }
        }

        private static void CreateFilters(SdkSession sessionCompany, List<Filter> filtersToCreate, List<Role> mainRoles,
            List<Role> rolesCompany)
        {
            string databaseName = sessionCompany.Sdk.GetUserInfo().DatabaseName;
            string sqlConString =
                $@"Data Source={_sqlDataSource};Initial Catalog={databaseName};User Id=sa;Password=Welkom@ridderiq";

            //Om de PK ook mee te nemen maken we de filters aan via SQL
            using (var conn = new SqlConnection(sqlConString))
            {
                conn.Open();

                try
                {
                    foreach (var filter in filtersToCreate)
                    {
                        int? roleCompany = null;
                        if (filter.Role.HasValue)
                        {
                            Role mainRole = mainRoles.First(x => x.RoleId == filter.Role);
                            if (!rolesCompany.Any(x =>
                                    String.Equals(x.Name, mainRole.Name, StringComparison.CurrentCultureIgnoreCase)))
                            {
                                Console.WriteLine(
                                    $"Rol {mainRole.Name} niet gevonden in {sessionCompany.Configuration.CompanyName}");
                                continue;
                            }

                            roleCompany = rolesCompany.First(x =>
                                String.Equals(x.Name, mainRole.Name, StringComparison.CurrentCultureIgnoreCase)).RoleId;
                        }


                        string sqlQueryInsertFilter =
                            "INSERT INTO R_TABLEQUERY([PK_R_TABLEQUERY], [FK_TABLEINFO], [DESIGNTARGET], [FK_ROLE], [SELECTIONNAME], [SELECTION], [EXTERNALKEY], [OLDID]) " +
                            "VALUES (@pkTablequery, @fkTableInfo, @Designtarget, @fkRole, @SelectionName, @Selection, @ExternalKey, @OldId);";

                        var sqlCmd = new SqlCommand(sqlQueryInsertFilter, conn);
                        sqlCmd.Parameters.AddWithValue("@pkTablequery", filter.FilterId);
                        sqlCmd.Parameters.AddWithValue("@fkTableInfo", filter.Table);
                        sqlCmd.Parameters.AddWithValue("@Designtarget", filter.DesignTarget);
                        sqlCmd.Parameters.AddWithValue("@fkRole",
                            filter.Role.HasValue ? (object)roleCompany : DBNull.Value);
                        sqlCmd.Parameters.AddWithValue("@SelectionName", filter.SelectionName);
                        sqlCmd.Parameters.AddWithValue("@Selection", filter.Selection);
                        sqlCmd.Parameters.AddWithValue("@ExternalKey", "CreatedBySql");
                        sqlCmd.Parameters.AddWithValue("@OldId", 0);

                        sqlCmd.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Het aanmaken van de filters via SQL is mislukt, oorzaak: {e}");
                }

                conn.Close();
            }

            //Werk de meta-info bij door alle aangemaakte records te wijzigen via een recordset
            var rsFilters = sessionCompany.GetRecordsetColumns("R_TABLEQUERY", "", "EXTERNALKEY = 'CreatedBySql'");
            rsFilters.UpdateWhenMoveRecord = false;
            rsFilters.UseDataChanges = false;

            rsFilters.MoveFirst();
            while (!rsFilters.EOF)
            {
                rsFilters.Fields["EXTERNALKEY"].Value = "ChangedByRecordset";
                rsFilters.MoveNext();
            }

            rsFilters.MoveFirst();

            try
            {
                var updateResult = rsFilters.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    Console.WriteLine(
                        $"Het toevoegen van filters is mislukt; oorzaak {updateResult.First(x => x.HasError).GetResult()}");
                }
                else
                {
                    Console.WriteLine($"Alle {rsFilters.RecordCount} records correct toegevoegd.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(
                    $"Het toevoegen van filters is mislukt; oorzaak {e}");
            }
        }

        private static void ChangeFilters(SdkSession sessionCompany, List<Filter> filtersToChange)
        {
            foreach (var batch in filtersToChange.Batch(2000))
            {
                // batch is een array van modellen 'Filters'.
                var rsFilters = sessionCompany.GetRecordsetColumns("R_TABLEQUERY", "PK_R_TABLEQUERY, SELECTION",
                    $"PK_R_TABLEQUERY IN ('{string.Join("','", batch.Select(x => x.FilterId))}')");
                rsFilters.UpdateWhenMoveRecord = false;
                rsFilters.UseDataChanges = false;

                rsFilters.MoveFirst();
                while (!rsFilters.EOF)
                {
                    var mainFilter = filtersToChange.First(x =>
                        x.FilterId == (Guid)rsFilters.Fields["PK_R_TABLEQUERY"].Value);

                    rsFilters.Fields["SELECTION"].Value = mainFilter.Selection;

                    rsFilters.MoveNext();
                }

                rsFilters.MoveFirst();

                try
                {
                    var updateResult = rsFilters.Update2();

                    if (updateResult.Any(x => x.HasError))
                    {
                        Console.WriteLine(
                            $"Het wijzigen van filters is mislukt; oorzaak {updateResult.First(x => x.HasError).GetResult()}");
                    }
                    else
                    {
                        Console.WriteLine($"Alle {rsFilters.RecordCount} records correct gewijzigd.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        $"Het wijzigen van filters is mislukt; oorzaak {e}");
                }
            }
        }

        private static ResultCompareLayouts CompareLayouts(List<Role> mainRoles, List<Layout> layoutsMainCompany,
            List<Role> rolesCompany, List<Layout> layoutsSessionCompany, string company)
        {
            var result = new ResultCompareLayouts();

            //Gebruikerslayouts verwijderen automatisch. Hoeven hier niet meegenomen te worden
            foreach (var layoutMainCompany in layoutsMainCompany.Where(x => x.DesignTarget != "User"))
            {
                if (layoutsSessionCompany.Any(x => x.SameLayout(layoutMainCompany)))
                {
                    Layout foundLayout = layoutsSessionCompany.First(x => x.SameLayout(layoutMainCompany));

                    if (!foundLayout.LayoutHasSameFormLayoutInfo(layoutMainCompany))
                    {
                        result.LayoutsToChange.Add(new LayoutToChange()
                        {
                            MainLayout = layoutMainCompany,
                            ChildLayout = foundLayout,
                        });
                    }
                }
                else
                {
                    result.LayoutsToCreate.Add(layoutMainCompany);
                }
            }

            //Get layouts not found in main
            foreach (var layoutSessionCompany in layoutsSessionCompany)
            {
                if (!layoutsMainCompany.Any(x => x.SameLayout(layoutSessionCompany)))
                {
                    result.LayoutsNotFoundInParent.Add(layoutSessionCompany);
                }
            }

            result.LayoutsNotFoundInParent.AddRange(
                layoutsSessionCompany.Where(x => x.DesignTarget == "User"));

            return result;
        }

        private static ResultCompareFilters CompareFilters(List<Role> mainRoles, List<Filter> filtersMainCompany,
            List<Role> rolesCompany, List<Filter> filtersSessionCompany, string company)
        {
            var result = new ResultCompareFilters();

            //Gebruikersfilters verwijderen automatisch. Hoeven hier niet meegenomen te worden
            foreach (var filterMainCompany in filtersMainCompany.Where(x => x.DesignTarget != DesignTarget.User))
            {
                if (filtersSessionCompany.Any(x => x.SameFilter(filterMainCompany)))
                {
                    Filter foundFilter = filtersSessionCompany.First(x => x.SameFilter(filterMainCompany));
                   
                    if (!foundFilter.FilterHasSameSelection(filterMainCompany))
                    {
                        result.FiltersToChange.Add(filterMainCompany);
                    }
                }
                else
                {
                    result.FiltersToCreate.Add(filterMainCompany);
                }
            }

            //Get filters not found in main
            var filtersNotFoundInParent = filtersSessionCompany.Select(x => x.FilterId)
                .Except(filtersMainCompany.Select(y => y.FilterId)).ToList();

            filtersNotFoundInParent.AddRange(filtersSessionCompany.Where(x => x.DesignTarget == DesignTarget.User)
                .Select(x => x.FilterId));

            if (filtersNotFoundInParent.Any())
            {
                result.FiltersNotFoundInParent.AddRange(filtersSessionCompany.Where(x =>
                    filtersNotFoundInParent.Contains(x.FilterId)));
            }

            return result;
        }


        private static List<Filter> GetFilters(SdkSession sessionCompany)
        {
            //1 = DesignTarget.System
            return sessionCompany.GetRecordsetColumns("R_TABLEQUERY",
                    "PK_R_TABLEQUERY, SELECTIONNAME, DESIGNTARGET, FK_TABLEINFO, FK_ROLE, SELECTION",
                    "DESIGNTARGET <> 1")
                .DataTable.AsEnumerable().Select(x => new Filter()
                {
                    FilterId = x.Field<Guid>("PK_R_TABLEQUERY"),
                    SelectionName = x.Field<string>("SELECTIONNAME"),
                    DesignTarget = x.Field<DesignTarget>("DESIGNTARGET"),
                    Table = x.Field<Guid>("FK_TABLEINFO"),
                    Role = x.Field<int?>("FK_ROLE"),
                    Selection = x.Field<Byte[]>("SELECTION")
                }).ToList();
        }

        private static List<Role> CompareRolesCompany(SdkSession sessionCompany, List<Role> mainRoles, string company,
            SdkSession mainSession)
        {
            var rolesCompany = GetRoles(sessionCompany);

            var roleNamesNotFound = GetRoleNamesNotFound(mainRoles, rolesCompany);

            if (roleNamesNotFound.Any())
            {
                var rsRolesCompany = sessionCompany.GetRecordsetColumns("R_ROLE", "", "PK_R_ROLE = -1");
                rsRolesCompany.UpdateWhenMoveRecord = true;
                rsRolesCompany.UseDataChanges = false;

                foreach (var roleName in roleNamesNotFound)
                {
                    rsRolesCompany.AddNew();
                    rsRolesCompany.Fields["NAME"].Value = roleName;

                    try
                    {
                        rsRolesCompany.Update2();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Het toevoegen van de rol {roleName} is mislukt; oorzaak {e}");
                        continue;
                    }

                    Console.WriteLine($"Rol {roleName} niet gevonden in {company}. Deze rol is toegevoegd.");
                }

                rolesCompany = GetRoles(sessionCompany);
            }

            var roleNamesNotFoundInParent = GetRoleNamesNotFound(rolesCompany, mainRoles);

            foreach (var roleName in roleNamesNotFoundInParent)
            {
                Console.WriteLine(
                    $"Rol {roleName} niet gevonden in {mainSession.Configuration.CompanyName}, wel in {company}.");
            }

            return rolesCompany;
        }

        private static bool ProcessRolePermisions(SdkSession sessionCompany, List<Role> mainRoles, List<RolePermission> mainRolePermissions,
            List<Role> rolesCompany, string company, SdkSession mainSession)
        {
            var rolePermissionsCompany = GetRolePermissions(sessionCompany).ToList();

            ResultComparePermissions resultComparePermissions = ComparePermissions(mainRoles, mainRolePermissions,
                rolesCompany, rolePermissionsCompany, company);

            Console.WriteLine(
                $"To change:{resultComparePermissions.PermissionsToChange.Count}; To create: {resultComparePermissions.PermissionsToCreate.Count}; Not found in parent: {resultComparePermissions.PermissionsNotFoundInParent.Count}");

            if (resultComparePermissions.PermissionsToChange.Any())
            {
                ChangeRolePermissions(sessionCompany, resultComparePermissions.PermissionsToChange);
            }

            if (resultComparePermissions.PermissionsToCreate.Any())
            {
                CreateRolePermissions(mainSession, sessionCompany, resultComparePermissions.PermissionsToCreate, mainRoles,
                    rolesCompany);
            }

            if (resultComparePermissions.PermissionsNotFoundInParent.Any())
            {
                List<RolePermission> sourceNotFoundInParentCompany = new List<RolePermission>();
                foreach (var rolePermissionNotFound in resultComparePermissions.PermissionsNotFoundInParent)
                {
                    if (!SourceExistsInCompany(sessionCompany, mainSession, rolePermissionNotFound))
                    {
                        sourceNotFoundInParentCompany.Add(rolePermissionNotFound);
                    }
                }

                List<Guid> toDeleteRolePermissionIds = resultComparePermissions.PermissionsNotFoundInParent
                    .Select(x => x.RolePermissionId)
                    .Except(sourceNotFoundInParentCompany.Select(x => x.RolePermissionId)).ToList();
                DeleteRolePermissions(sessionCompany, toDeleteRolePermissionIds);
            }

            return resultComparePermissions.PermissionsToChange.Any() ||
                   resultComparePermissions.PermissionsToCreate.Any();
        }

        private static void DeleteRolePermissions(SdkSession sessionCompany, List<Guid> toDeleteRolePermissionIds)
        {
            foreach (var batch in toDeleteRolePermissionIds.Batch(2000))
            {
                // batch is een array van modellen 'RolePermissionToChange'.
                var rsRolePermissions = sessionCompany.GetRecordsetColumns("R_ROLEPERMISSION", "",
                    $"PK_R_ROLEPERMISSION IN ('{string.Join("','", batch.Select(x => x))}')");
                rsRolePermissions.UpdateWhenMoveRecord = false;
                rsRolePermissions.UseDataChanges = false;

                rsRolePermissions.MoveFirst();
                while (!rsRolePermissions.EOF)
                {
                    rsRolePermissions.Delete();
                    rsRolePermissions.MoveNext();
                }

                rsRolePermissions.MoveFirst();

                try
                {
                    rsRolePermissions.Update2();
                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        $"Het verwijderen van rechten is mislukt; oorzaak {e}");
                }
            }
        }

        private static void DeleteLayouts(SdkSession sessionCompany, List<Layout> toDeleteLayouts)
        {
            string databaseName = sessionCompany.Sdk.GetUserInfo().DatabaseName;
            string sqlConString =
                $@"Data Source={_sqlDataSource};Initial Catalog={databaseName};User Id=sa;Password=Welkom@ridderiq";

            try
            {
                //We hebben geen schrijfrechten op de tabel 'R_FORMSETTING'. Daarom via SQL
                using (var conn = new SqlConnection(sqlConString))
                {
                    conn.Open();


                    string sqlQueryDeleteLayouts =
                        "DELETE FROM R_FORMSETTING " +
                        "WHERE PK_R_FORMSETTING IN (";

                    for (var i = 0; i < toDeleteLayouts.Count; i++)
                    {
                        sqlQueryDeleteLayouts += (i == 0) ? $"@PksLayoutsToDelete{i}" : $", @PksLayoutsToDelete{i}";
                    }

                    sqlQueryDeleteLayouts += ")";

                    var sqlCmd = new SqlCommand(sqlQueryDeleteLayouts, conn);

                    for (var i = 0; i < toDeleteLayouts.Count; i++)
                    {
                        sqlCmd.Parameters.AddWithValue($"@PksLayoutsToDelete{i}", toDeleteLayouts[i].LayoutId);
                    }

                    sqlCmd.ExecuteNonQuery();

                    conn.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Het verwijderen van layouts via SQL is mislukt, oorzaak: {e}");
            }

            /*
            //We hebben geen schrijfrechten op de tabel 'R_FORMSETTING'. Daarom via SQL en staat onderstaande uit
            foreach (var batch in toDeleteLayouts.Batch(2000))
            {
                var rsLayouts = sessionCompany.GetRecordsetColumns("R_FORMSETTING", "",
                    $"PK_R_FORMSETTING IN ('{string.Join("','", batch.Select(x => x.LayoutId))}')");
                rsLayouts.UpdateWhenMoveRecord = false;
                rsLayouts.UseDataChanges = false;

                rsLayouts.MoveFirst();
                while (!rsLayouts.EOF)
                {
                    rsLayouts.Delete();
                    rsLayouts.MoveNext();
                }

                rsLayouts.MoveFirst();

                try
                {
                    rsLayouts.Update2();
                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        $"Het verwijderen van layouts is mislukt; oorzaak {e}");
                }
            }*/
        }

        private static List<string> GetRoleNamesNotFound(List<Role> parentList, List<Role> childList)
        {
            List<string> result = new List<string>();

            foreach (var item in parentList)
            {
                if (!childList.Select(x => x.Name).Contains(item.Name, StringComparer.OrdinalIgnoreCase))
                {
                    result.Add(item.Name);
                }
            }

            return result;
        }

        private static void CreateNewRoleAndUserToRefreshPermissions(SdkSession sessionCompany)
        {
            var rsUser = sessionCompany.GetRecordsetColumns("R_USER", "", "PK_R_USER = -1");
            rsUser.AddNew();
            rsUser.Fields["USERNAME"].Value = "REFRESHPERMISSIONS";
            rsUser.Fields["FK_LANGUAGE"].Value = 1;
            var resultUser = rsUser.Update2();

            if (resultUser.Any(x => x.HasError))
            {
                Console.WriteLine($"Het toevoegen van een nieuwe gebruiker is mislukt; oorzaak {resultUser.First(x => x.HasError).GetResult()}");
            }

            var rsRole = sessionCompany.GetRecordsetColumns("R_ROLE", "", "PK_R_ROLE = -1");
            rsRole.AddNew();
            rsRole.Fields["NAME"].Value = "REFRESHPERMISSIONS";
            var resultRole = rsRole.Update2();

            if (resultRole.Any(x => x.HasError))
            {
                Console.WriteLine($"Het toevoegen van een nieuwe rol is mislukt; oorzaak {resultRole.First(x => x.HasError).GetResult()}");
            }

            var rsRoleUser = sessionCompany.GetRecordsetColumns("R_ROLEUSER", "", "PK_R_ROLEUSER = -1");
            rsRoleUser.AddNew();
            rsRoleUser.Fields["FK_R_ROLE"].Value = resultRole.First().PrimaryKey;
            rsRoleUser.Fields["FK_R_USER"].Value = resultUser.First().PrimaryKey;

            var resultRoleUser = rsRoleUser.Update2();

            if (resultRoleUser.Any(x => x.HasError))
            {
                Console.WriteLine($"Het koppelen van de nieuwe gebruiker aan de nieuwe rol is mislukt; oorzaak {resultRoleUser.First(x => x.HasError).GetResult()}");
            }

            //Verwijder de temp-rollen en users
            var rsRoleDelete = sessionCompany.GetRecordsetColumnsSorted("R_ROLE", "",
                $"PK_R_ROLE = {resultRole.First().PrimaryKey}", "");
            rsRoleDelete.MoveFirst();
            rsRoleDelete.Delete();
            var resultDeleteRole = rsRoleDelete.Update2();

            if (resultDeleteRole.Any(x => x.HasError))
            {
                Console.WriteLine($"Het verwijderen van de nieuwe rol is mislukt; oorzaak {resultDeleteRole.First(x => x.HasError).GetResult()}");
            }

            var rsUserDelete = sessionCompany.GetRecordsetColumnsSorted("R_USER", "",
                $"PK_R_USER = {resultUser.First().PrimaryKey}", "");
            rsUserDelete.MoveFirst();
            rsUserDelete.Delete();
            var resultDeleteUser = rsUserDelete.Update2();

            if (resultDeleteUser.Any(x => x.HasError))
            {
                Console.WriteLine($"Het verwijderen van de nieuwe gebruiker is mislukt; oorzaak {resultDeleteUser.First(x => x.HasError).GetResult()}");
            }
        }

        private static void CreateLayouts(SdkSession sessionCompany,
            List<Layout> layoutsToCreate, List<Role> mainRoles, List<Role> rolesCompany)
        {
            string databaseName = sessionCompany.Sdk.GetUserInfo().DatabaseName;
            string sqlConString =
                $@"Data Source={_sqlDataSource};Initial Catalog={databaseName};User Id=sa;Password=Welkom@ridderiq";

            try
            {
                //We hebben geen schrijfrechten op de tabel 'R_FORMSETTING' ontvangen vanuit de ontwikkelafdeling. Daarom via SQL
                using (var conn = new SqlConnection(sqlConString))
                {
                    conn.Open();

                    foreach (var layoutToCreate in layoutsToCreate)
                    {
                        //Zoek naar rol indien nodig
                        int? roleCompany = null;
                        if (layoutToCreate.Role != null)
                        {
                            var mainRole = mainRoles.First(x => x.RoleId == layoutToCreate.Role.RoleId);
                            if (!rolesCompany.Any(x =>
                                String.Equals(x.Name, mainRole.Name, StringComparison.CurrentCultureIgnoreCase)))
                            {
                                Console.WriteLine(
                                    $"Rol {mainRole.Name} niet gevonden in {sessionCompany.Configuration.CompanyName}");
                                continue;
                            }

                            roleCompany = rolesCompany.First(x =>
                                String.Equals(x.Name, mainRole.Name, StringComparison.CurrentCultureIgnoreCase)).RoleId;
                        }

                        string sqlQueryInsertLayout =
                            "INSERT INTO R_FORMSETTING([LAYOUTNAME], [DESIGNTARGET], [NAME], [FORMTYPE], [CONTEXT], [FK_ROLE], [FORMLAYOUTINFO], [FORMSETTINGINFO]) " +
                            "VALUES (@LayoutName, @Designtarget, @Name, @FormType, @Context, @FkRole, @FormLayoutInfo, @FormSettingInfo);";

                        var sqlCmd = new SqlCommand(sqlQueryInsertLayout, conn);
                        sqlCmd.Parameters.AddWithValue("@LayoutName", layoutToCreate.LayoutName);
                        sqlCmd.Parameters.AddWithValue("@Designtarget", layoutToCreate.DesignTarget);
                        sqlCmd.Parameters.AddWithValue("@Name", layoutToCreate.Name);
                        sqlCmd.Parameters.AddWithValue("@FormType", layoutToCreate.FormType);
                        sqlCmd.Parameters.AddWithValue("@Context", layoutToCreate.Context);
                        sqlCmd.Parameters.AddWithValue("@FkRole",
                            layoutToCreate.Role != null
                                ? (object)roleCompany.Value
                                : DBNull.Value);
                        sqlCmd.Parameters.AddWithValue("@FormLayoutInfo", layoutToCreate.FormLayoutInfo);
                        sqlCmd.Parameters.AddWithValue("@FormSettingInfo", layoutToCreate.FormSettingInfo);

                        sqlCmd.ExecuteNonQuery();
                    }

                    conn.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Het toevoegen van layouts via SQL is mislukt, oorzaak: {e}");
            }

            /*
            //We hebben geen schrijfrechten op de tabel 'R_FORMSETTING' ontvangen vanuit de ontwikkelafdeling. Daarom staat dit uit

            var rsLayouts = sessionCompany.GetRecordsetColumns("R_FORMSETTING", "", "PK_R_FORMSETTING = -1");
            rsLayouts.UpdateWhenMoveRecord = false;
            rsLayouts.UseDataChanges = false;

            foreach (var layoutToCreate in layoutsToCreate)
            {
                //Zoek naar rol indien nodig
                Role mainRole = null;
                if (layoutToCreate.Role != null)
                {
                    mainRole = mainRoles.First(x => x.RoleId == layoutToCreate.Role.RoleId);
                    if (rolesCompany.Count(x =>
                            String.Equals(x.Name, mainRole.Name, StringComparison.CurrentCultureIgnoreCase)) == 0)
                    {
                        Console.WriteLine(
                            $"Rol {mainRole.Name} niet gevonden in {sessionCompany.Configuration.CompanyName}");
                        continue;
                    }
                }

                rsLayouts.AddNew();
                rsLayouts.Fields["LAYOUTNAME"].Value = layoutToCreate.LayoutName;
                rsLayouts.Fields["DESIGNTARGET"].Value = layoutToCreate.DesignTarget;
                rsLayouts.Fields["NAME"].Value = layoutToCreate.Name;
                rsLayouts.Fields["FORMTYPE"].Value = layoutToCreate.FormType;
                rsLayouts.Fields["CONTEXT"].Value = layoutToCreate.Context;

                if (layoutToCreate.Role != null)
                {
                    rsLayouts.Fields["FK_ROLE"].Value = rolesCompany.First(x =>
                        String.Equals(x.Name, mainRole.Name, StringComparison.CurrentCultureIgnoreCase)).RoleId;
                }

                rsLayouts.Fields["FORMLAYOUTINFO"].Value = layoutToCreate.FormLayoutInfo;
                rsLayouts.Fields["FORMSETTINGINFO"].Value = layoutToCreate.FormSettingInfo;
            }

            try
            {
                var updateResult = rsLayouts.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    Console.WriteLine(
                        $"Het toevoegen van layouts is mislukt; oorzaak {updateResult.First(x => x.HasError).GetResult()}");
                }
                else
                {
                    Console.WriteLine($"Alle {rsLayouts.RecordCount} records correct toegevoegd.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(
                    $"Het toevoegen van layouts is mislukt; oorzaak {e}");
            }*/
        }

        private static void CreateRolePermissions(SdkSession mainSession, SdkSession sessionCompany, List<RolePermission> permissionsToCreate, List<Role> mainRoles, List<Role> rolesCompany)
        {
            var rsRolePermissions = sessionCompany.GetRecordsetColumns("R_ROLEPERMISSION", "",
                "PK_R_ROLEPERMISSION IS NULL");
            rsRolePermissions.UpdateWhenMoveRecord = false;
            rsRolePermissions.UseDataChanges = false;

            var widgets = sessionCompany.GetRecordsetColumns("R_IQHOMEWIDGET", "PK_R_IQHOMEWIDGET, NAME", "", "")
                .DataTable.AsEnumerable().Select(x => new Widget()
                {
                    WidgetId = x.Field<Guid>("PK_R_IQHOMEWIDGET"),
                    Name = x.Field<string>("NAME"),
                });

            foreach (var rolePermission in permissionsToCreate)
            {
                if (!SourceExistsInCompany(mainSession, sessionCompany, rolePermission))
                {
                    continue;
                }

                Role mainRole = mainRoles.First(x => x.RoleId == rolePermission.Role);
                if (rolesCompany.Count(x => String.Equals(x.Name, mainRole.Name, StringComparison.CurrentCultureIgnoreCase)) == 0)
                {
                    Console.WriteLine($"Rol {mainRole.Name} niet gevonden in {sessionCompany.Configuration.CompanyName}");
                    continue;
                }

                rsRolePermissions.AddNew();
                rsRolePermissions.Fields["FK_ROLE"].Value = rolesCompany.First(x => String.Equals(x.Name, mainRole.Name, StringComparison.CurrentCultureIgnoreCase)).RoleId;
                rsRolePermissions.Fields["TABLEGROUPTYPE"].Value = rolePermission.TableGroupType;
                rsRolePermissions.Fields["SCOPE"].Value = rolePermission.Scope;

                rsRolePermissions.Fields["DIRECT"].Value = rolePermission.Direct;
                rsRolePermissions.Fields["READ"].Value = rolePermission.Read;
                rsRolePermissions.Fields["WRITE"].Value = rolePermission.Write;
                rsRolePermissions.Fields["INSERT"].Value = rolePermission.Insert;
                rsRolePermissions.Fields["DELETE"].Value = rolePermission.Delete;
                rsRolePermissions.Fields["ALL"].Value = rolePermission.All;

                rsRolePermissions.Fields["FK_PERMISSIONGROUP"].Value = (rolePermission.PermissionGroup.HasValue) ? (object)rolePermission.PermissionGroup : DBNull.Value;
                rsRolePermissions.Fields["FK_TABLEINFO"].Value = (rolePermission.TableInfo.HasValue) ? (object)rolePermission.TableInfo : DBNull.Value;
                rsRolePermissions.Fields["FK_COLUMNINFO"].Value = (rolePermission.ColumnInfo.HasValue) ? (object)rolePermission.ColumnInfo : DBNull.Value;
                rsRolePermissions.Fields["FK_WORKFLOWEVENT"].Value = (rolePermission.WorkflowEvent.HasValue) ? (object)rolePermission.WorkflowEvent : DBNull.Value;
                rsRolePermissions.Fields["FK_SYSTEMREPORT"].Value = (rolePermission.SystemReport.HasValue) ? (object)rolePermission.SystemReport : DBNull.Value;
                rsRolePermissions.Fields["FK_CUSTOMREPORT"].Value = (rolePermission.CustomReport.HasValue) ? (object)rolePermission.CustomReport : DBNull.Value;
                rsRolePermissions.Fields["FK_USERREPORT"].Value = (rolePermission.UserReport.HasValue) ? (object)rolePermission.UserReport : DBNull.Value;
                rsRolePermissions.Fields["FK_SYSTEMSCRIPT"].Value = (rolePermission.SystemScript.HasValue) ? (object)rolePermission.SystemScript : DBNull.Value;
                rsRolePermissions.Fields["FK_CUSTOMSCRIPT"].Value = (rolePermission.CustomScript.HasValue) ? (object)rolePermission.CustomScript : DBNull.Value;
                rsRolePermissions.Fields["FK_USERSCRIPT"].Value = (rolePermission.UserScript.HasValue) ? (object)rolePermission.UserScript : DBNull.Value;

                if (rolePermission.HomeWidget != "")
                {
                    rsRolePermissions.Fields["FK_IQHOMEWIDGET"].Value = widgets.First(x => x.Name == rolePermission.HomeWidget).WidgetId;
                }
                
                rsRolePermissions.Fields["QLIKAPPID"].Value = rolePermission.QlikApp;
                rsRolePermissions.Fields["CUSTOM"].Value = rolePermission.Custom;
                rsRolePermissions.Fields["UIACTION"].Value = (rolePermission.UIAction.HasValue) ? (object)rolePermission.UIAction : DBNull.Value;
                rsRolePermissions.Fields["ACTIONID"].Value = rolePermission.ActionId;
                rsRolePermissions.Fields["FORMPARTID"].Value = rolePermission.FormPart;

                if (rolePermission.FilterName != "")
                {
                    rsRolePermissions.Fields["FILTERNAME"].Value = rolePermission.FilterName;
                    rsRolePermissions.Fields["RECORDFILTER"].Value = rolePermission.RecordFilter;
                }
            }

            try
            {
                var updateResult = rsRolePermissions.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    Console.WriteLine(
                        $"Het toevoegen van rechten is mislukt; oorzaak {updateResult.First(x => x.HasError).GetResult()}");
                }
                else
                {
                    Console.WriteLine($"Alle {rsRolePermissions.RecordCount} records correct toegevoegd.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(
                    $"Het toevoegen van rechten is mislukt; oorzaak {e}");
            }
        }

        private static bool SourceExistsInCompany(SdkSession parentCompany, SdkSession childCompany, RolePermission rolePermission)
        {
            string recordtagRolePermission = DetermineRecordtagRolePermission(parentCompany, rolePermission);

            if (rolePermission.TableInfo.HasValue)
            {
                var rsTableInfo = childCompany.GetRecordsetColumns("M_TABLEINFO", "PK_M_TABLEINFO",
                    $"PK_M_TABLEINFO = '{rolePermission.TableInfo.Value}'");

                if (rsTableInfo.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (rolePermission.ColumnInfo.HasValue)
            {
                var rsColumnInfo = childCompany.GetRecordsetColumns("M_COLUMNINFO", "PK_M_COLUMNINFO",
                    $"PK_M_COLUMNINFO = '{rolePermission.ColumnInfo.Value}'");

                if (rsColumnInfo.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (rolePermission.WorkflowEvent.HasValue)
            {
                var rsWorkflowEvents = childCompany.GetRecordsetColumns("M_WORKFLOWEVENT", "PK_M_WORKFLOWEVENT",
                    $"PK_M_WORKFLOWEVENT = '{rolePermission.WorkflowEvent.Value}'");

                if (rsWorkflowEvents.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (rolePermission.SystemReport.HasValue)
            {
                var rsReports = childCompany.GetRecordsetColumns("S_REPORT", "PK_S_REPORT",
                    $"PK_S_REPORT = '{rolePermission.SystemReport.Value}'");

                if (rsReports.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (rolePermission.CustomReport.HasValue)
            {
                var rsReports = childCompany.GetRecordsetColumns("R_CUSTOMREPORT", "PK_R_CUSTOMREPORT",
                    $"PK_R_CUSTOMREPORT = '{rolePermission.CustomReport.Value}'");

                if (rsReports.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (rolePermission.UserReport.HasValue)
            {
                var rsReports = childCompany.GetRecordsetColumns("R_USERREPORT", "PK_R_USERREPORT",
                    $"PK_R_USERREPORT = '{rolePermission.UserReport.Value}'");

                if (rsReports.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (rolePermission.SystemScript.HasValue)
            {
                var rsScripts = childCompany.GetRecordsetColumns("S_SCRIPT", "PK_S_SCRIPT",
                    $"PK_S_SCRIPT = '{rolePermission.SystemScript.Value}'");

                if (rsScripts.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (rolePermission.CustomScript.HasValue)
            {
                var rsScripts = childCompany.GetRecordsetColumns("R_CUSTOMSCRIPT", "PK_R_CUSTOMSCRIPT",
                    $"PK_R_CUSTOMSCRIPT = '{rolePermission.CustomScript.Value}'");

                if (rsScripts.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (rolePermission.UserScript.HasValue)
            {
                var rsScripts = childCompany.GetRecordsetColumns("R_USERSCRIPT", "PK_R_USERSCRIPT",
                    $"PK_R_USERSCRIPT = '{rolePermission.UserScript.Value}'");

                if (rsScripts.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (rolePermission.HomeWidget != "")
            {
                var rsWidgets = childCompany.GetRecordsetColumns("R_IQHOMEWIDGET", "PK_R_IQHOMEWIDGET",
                    $"NAME = '{rolePermission.HomeWidget}'");

                if (rsWidgets.RecordCount == 0)
                {
                    Console.WriteLine($"{recordtagRolePermission} niet gevonden in {childCompany.Configuration.CompanyName}");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        private static string DetermineRecordtagRolePermission(SdkSession session, RolePermission rolePermission)
        {
            if (rolePermission.TableInfo.HasValue)
            {
                var rsTableInfoMain = session.GetRecordsetColumns("M_TABLEINFO", "TABLENAME",
                    $"PK_M_TABLEINFO = '{rolePermission.TableInfo.Value}'");
                rsTableInfoMain.MoveFirst();

                return $"Tabel {rsTableInfoMain.Fields["TABLENAME"].Value}";
            }
            else if (rolePermission.ColumnInfo.HasValue)
            {
                var rsColumnInfoMain = session.GetRecordsetColumns("M_COLUMNINFO", "COLUMNNAME, FK_TABLEINFO",
                    $"PK_M_COLUMNINFO = '{rolePermission.ColumnInfo.Value}'");
                rsColumnInfoMain.MoveFirst();

                var rsTableInfoMain = session.GetRecordsetColumns("M_TABLEINFO", "TABLENAME",
                    $"PK_M_TABLEINFO = '{rsColumnInfoMain.Fields["FK_TABLEINFO"].Value}'");
                rsTableInfoMain.MoveFirst();

                return $"Kolom {rsColumnInfoMain.Fields["COLUMNNAME"].Value} in tabel {rsTableInfoMain.Fields["TABLENAME"].Value}";
            }
            else if (rolePermission.WorkflowEvent.HasValue)
            {
                var rsWorkflowEventsMain = session.GetRecordsetColumns("M_WORKFLOWEVENT", "CAPTION",
                    $"PK_M_WORKFLOWEVENT = '{rolePermission.WorkflowEvent.Value}'");
                rsWorkflowEventsMain.MoveFirst();

                return $"Workflow {rsWorkflowEventsMain.Fields["CAPTION"].Value}";
            }
            else if (rolePermission.SystemReport.HasValue)
            {
                var rsReportsMain = session.GetRecordsetColumns("S_REPORT", "REPORTNAME",
                    $"PK_S_REPORT = '{rolePermission.SystemReport.Value}'");
                rsReportsMain.MoveFirst();

                return $"Rapport {rsReportsMain.Fields["REPORTNAME"].Value}";
            }
            else if (rolePermission.CustomReport.HasValue)
            {
                var rsReportsMain = session.GetRecordsetColumns("R_CUSTOMREPORT", "REPORTNAME",
                    $"PK_R_CUSTOMREPORT = '{rolePermission.CustomReport.Value}'");
                rsReportsMain.MoveFirst();

                return $"Rapport {rsReportsMain.Fields["REPORTNAME"].Value}";
            }
            else if (rolePermission.UserReport.HasValue)
            {
                var rsReportsMain = session.GetRecordsetColumns("R_USERREPORT", "REPORTNAME",
                    $"PK_R_USERREPORT = '{rolePermission.UserReport.Value}'");
                rsReportsMain.MoveFirst();

                return $"Rapport {rsReportsMain.Fields["REPORTNAME"].Value}";
            }
            else if (rolePermission.SystemScript.HasValue)
            {
                var rsScripts = session.GetRecordsetColumns("S_SCRIPT", "NAME",
                    $"PK_S_SCRIPT = '{rolePermission.SystemScript.Value}'");
                rsScripts.MoveFirst();

                return $"Script {rsScripts.Fields["NAME"].Value}";
            }
            else if (rolePermission.CustomScript.HasValue)
            {
                var rsScripts = session.GetRecordsetColumns("R_CUSTOMSCRIPT", "NAME",
                    $"PK_R_CUSTOMSCRIPT = '{rolePermission.CustomScript.Value}'");
                rsScripts.MoveFirst();

                return $"Script {rsScripts.Fields["NAME"].Value}";
            }
            else if (rolePermission.UserScript.HasValue)
            {
                var rsScripts = session.GetRecordsetColumns("R_USERSCRIPT", "NAME",
                    $"PK_R_USERSCRIPT = '{rolePermission.UserScript.Value}'");
                rsScripts.MoveFirst();

                return $"Script {rsScripts.Fields["NAME"].Value}";
            }
            else if (rolePermission.HomeWidget != "")
            {
                return $"IQ Home widget {rolePermission.HomeWidget}";
            }
            else
            {
                return "";
            }
        }

        private static void ChangeLayouts(SdkSession sessionCompany, List<LayoutToChange> layoutsToChange)
        {
            string databaseName = sessionCompany.Sdk.GetUserInfo().DatabaseName;
            string sqlConString =
                $@"Data Source={_sqlDataSource};Initial Catalog={databaseName};User Id=sa;Password=Welkom@ridderiq";

            try
            {
                //We hebben geen schrijfrechten op de tabel 'R_FORMSETTING'. Daarom via SQL
                using (var conn = new SqlConnection(sqlConString))
                {
                    conn.Open();

                    foreach (var layoutToChange in layoutsToChange)
                    {
                        string sqlQueryChangeLayout =
                            "UPDATE R_FORMSETTING " +
                            "SET [FORMLAYOUTINFO] = @FormLayoutInfo, [FORMSETTINGINFO] = @FormSettingInfo " +
                            "WHERE PK_R_FORMSETTING = @PkChildFormSetting;";

                        var sqlCmd = new SqlCommand(sqlQueryChangeLayout, conn);

                        sqlCmd.Parameters.AddWithValue("@FormLayoutInfo", layoutToChange.MainLayout.FormLayoutInfo);
                        sqlCmd.Parameters.AddWithValue("@FormSettingInfo", layoutToChange.MainLayout.FormSettingInfo);
                        sqlCmd.Parameters.AddWithValue("@PkChildFormSetting", layoutToChange.ChildLayout.LayoutId);

                        sqlCmd.ExecuteNonQuery();
                    }

                    conn.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Het wijzigen van layouts via SQL is mislukt, oorzaak: {e}");
            }

            //Via de SDK mogen we de tabel 'R_FORMSETTING' niet wijzigen, daarom via SQL en staat onderstaande uit
            /*
            foreach (var batch in layoutsToChange.Batch(2000))
            {
                // batch is een array van modellen 'Layouts'.
                var rsLayouts = sessionCompany.GetRecordsetColumns("R_FORMSETTING", "",
                    $"PK_R_FORMSETTING IN ({string.Join(",", batch.Select(x => x.ChildLayout.LayoutId))})");
                rsLayouts.UpdateWhenMoveRecord = false;
                rsLayouts.UseDataChanges = false;

                while (!rsLayouts.EOF)
                {
                    var layoutMainCompany = layoutsToChange.First(x =>
                        x.ChildLayout.LayoutId == (int)rsLayouts.Fields["PK_R_FORMSETTING"].Value).MainLayout;

                    rsLayouts.Fields["FORMLAYOUTINFO"].Value = layoutMainCompany.FormLayoutInfo;
                    rsLayouts.Fields["FORMSETTINGINFO"].Value = layoutMainCompany.FormSettingInfo;

                    rsLayouts.MoveNext();
                }

                rsLayouts.MoveFirst();

                try
                {
                    var updateResult = rsLayouts.Update2();

                    if (updateResult.Any(x => x.HasError))
                    {
                        Console.WriteLine(
                            $"Het wijzigen van layouts is mislukt; oorzaak {updateResult.First(x => x.HasError).GetResult()}");
                    }
                    else
                    {
                        Console.WriteLine($"Alle {rsLayouts.RecordCount} records correct gewijzigd.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        $"Het wijzigen van rechten is mislukt; oorzaak {e}");
                }
            }*/
        }

        private static void ChangeRolePermissions(SdkSession sessionCompany, List<RolePermissionToChange> permissionsToChange)
        {
            foreach (var batch in permissionsToChange.Batch(2000))
            {
                // batch is een array van modellen 'RolePermissionToChange'.
                var rsRolePermissions = sessionCompany.GetRecordsetColumns("R_ROLEPERMISSION", "",
                    $"PK_R_ROLEPERMISSION IN ('{string.Join("','", batch.Select(x => x.ChildRolePermission.RolePermissionId))}')");
                rsRolePermissions.UpdateWhenMoveRecord = false;
                rsRolePermissions.UseDataChanges = false;

                rsRolePermissions.MoveFirst();
                while (!rsRolePermissions.EOF)
                {
                    var mainPermission = permissionsToChange
                        .First(x => x.ChildRolePermission.RolePermissionId ==
                                    (Guid)rsRolePermissions.Fields["PK_R_ROLEPERMISSION"].Value)
                        .MainPermission;

                    rsRolePermissions.Fields["DIRECT"].Value = mainPermission.Direct;
                    rsRolePermissions.Fields["READ"].Value = mainPermission.Read;
                    rsRolePermissions.Fields["WRITE"].Value = mainPermission.Write;
                    rsRolePermissions.Fields["INSERT"].Value = mainPermission.Insert;
                    rsRolePermissions.Fields["DELETE"].Value = mainPermission.Delete;
                    rsRolePermissions.Fields["ALL"].Value = mainPermission.All;

                    //Table group type = filter
                    if ((int)rsRolePermissions.Fields["TABLEGROUPTYPE"].Value == 6)
                    {
                        rsRolePermissions.Fields["FILTERNAME"].Value = mainPermission.FilterName;
                        rsRolePermissions.Fields["RECORDFILTER"].Value = mainPermission.RecordFilter;
                    }

                    rsRolePermissions.MoveNext();
                }

                rsRolePermissions.MoveFirst();

                try
                {
                    var updateResult = rsRolePermissions.Update2();

                    if (updateResult.Any(x => x.HasError))
                    {
                        Console.WriteLine(
                            $"Het wijzigen van rechten is mislukt; oorzaak {updateResult.First(x => x.HasError).GetResult()}");
                    }
                    else
                    {
                        Console.WriteLine($"Alle {rsRolePermissions.RecordCount} records correct gewijzigd.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(
                        $"Het wijzigen van rechten is mislukt; oorzaak {e}");
                }
            }
        }

        private static ResultComparePermissions ComparePermissions(List<Role> mainRoles, List<RolePermission> mainRolePermissions,
            List<Role> rolesCompany, List<RolePermission> rolePermissionsCompany, string companyName)
        {
            var result = new ResultComparePermissions();

            foreach (var mainRolePermission in mainRolePermissions)
            {
                Role mainRole = mainRoles.First(x => x.RoleId == mainRolePermission.Role);
                Role roleCompany = rolesCompany.First(x => String.Equals(x.Name, mainRole.Name, StringComparison.CurrentCultureIgnoreCase));

                if (rolePermissionsCompany.Any(x => x.Role == roleCompany.RoleId && x.SameRolePermission(mainRolePermission)))
                {
                    RolePermission foundRolePermission =
                        rolePermissionsCompany.First(x => x.Role == roleCompany.RoleId && x.SameRolePermission(mainRolePermission));

                    if (!foundRolePermission.RolePermissionHasSamePermissions(mainRolePermission))
                    {
                        result.PermissionsToChange.Add(new RolePermissionToChange()
                        {
                            MainPermission = mainRolePermission,
                            ChildRolePermission = foundRolePermission,
                        });
                    }
                }
                else
                {
                    result.PermissionsToCreate.Add(mainRolePermission);
                }
            }

            //Get permissions not found in parent
            foreach (var companyRolePermission in rolePermissionsCompany)
            {
                Role companyRole = rolesCompany.First(x => x.RoleId == companyRolePermission.Role);
                Role roleMain = mainRoles.FirstOrDefault(x =>
                    String.Equals(x.Name, companyRole.Name, StringComparison.CurrentCultureIgnoreCase));

                if (roleMain == null)
                {
                    continue;
                }

                if (!mainRolePermissions.Any(x =>
                    x.Role == roleMain.RoleId && x.SameRolePermission(companyRolePermission)))
                {
                    result.PermissionsNotFoundInParent.Add(companyRolePermission);
                }
            }

            return result;
        }

        public class ResultComparePermissions
        {
            public List<RolePermissionToChange> PermissionsToChange { get; set; }
            public List<RolePermission> PermissionsToCreate { get; set; }

            public List<RolePermission> PermissionsNotFoundInParent { get; set; }

            public ResultComparePermissions()
            {
                PermissionsToChange = new List<RolePermissionToChange>();
                PermissionsToCreate = new List<RolePermission>();
                PermissionsNotFoundInParent = new List<RolePermission>();
            }
        }

        public class ResultCompareFilters
        {
            public List<Filter> FiltersToChange { get; set; }
            public List<Filter> FiltersToCreate { get; set; }

            public List<Filter> FiltersNotFoundInParent { get; set; }

            public ResultCompareFilters()
            {
                FiltersToChange = new List<Filter>();
                FiltersToCreate = new List<Filter>();
                FiltersNotFoundInParent = new List<Filter>();
            }
        }

        public class ResultCompareLayouts
        {
            public List<LayoutToChange> LayoutsToChange { get; set; }
            public List<Layout> LayoutsToCreate { get; set; }

            public List<Layout> LayoutsNotFoundInParent { get; set; }

            public ResultCompareLayouts()
            {
                LayoutsToChange = new List<LayoutToChange>();
                LayoutsToCreate = new List<Layout>();
                LayoutsNotFoundInParent = new List<Layout>();
            }
        }

        public class LayoutToChange
        {
            public Layout MainLayout { get; set; }
            public Layout ChildLayout { get; set; }
        }

        public class RolePermissionToChange
        {
            public RolePermission MainPermission { get; set; }
            public RolePermission ChildRolePermission { get; set; }
        }

        public class MailReportSettingToChange
        {
            public MailReportSetting MainMailReportSetting { get; set; }
            public MailReportSetting ChildMailReportSetting { get; set; }
        }

        private static List<Role> GetRoles(SdkSession mainSession)
        {
            return mainSession.GetRecordsetColumns("R_ROLE", "PK_R_ROLE, NAME", "")
                .DataTable.AsEnumerable().Select(x => new Role()
                {
                    RoleId = x.Field<int>("PK_R_ROLE"),
                    Name = x.Field<string>("NAME"),
                }).ToList();
        }

        private static List<RolePermission> GetRolePermissions(SdkSession mainSession)
        {
            var tablesToExclude = mainSession.GetRecordsetColumns("M_TABLEINFO", "PK_M_TABLEINFO",
                $"TABLENAME LIKE 'L%' OR TABLENAME LIKE 'X%'").DataTable.AsEnumerable().Select(x => x.Field<Guid>("PK_M_TABLEINFO")).ToList();

            var columnsToExclude = mainSession.GetRecordsetColumns("M_COLUMNINFO", "PK_M_COLUMNINFO",
                    $"COLUMNNAME IN ('CREATOR', 'DATECHANGED', 'DATECREATED', 'EXTERNALKEY', 'FK_WORKFLOWSTATE', 'RECORDLINK', 'USERCHANGED')")
                .DataTable.AsEnumerable().Select(x => x.Field<Guid>("PK_M_COLUMNINFO")).ToList();

            var widgets = mainSession.GetRecordsetColumns("R_IQHOMEWIDGET", "PK_R_IQHOMEWIDGET, NAME", "", "")
                .DataTable.AsEnumerable().Select(x => new Widget()
                {
                    WidgetId = x.Field<Guid>("PK_R_IQHOMEWIDGET"),
                    Name = x.Field<string>("NAME"),
                });

            return mainSession.GetRecordsetColumns("R_ROLEPERMISSION", "", "")
                .DataTable.AsEnumerable().Select(x => new RolePermission()
                {
                    RolePermissionId = x.Field<Guid>("PK_R_ROLEPERMISSION"),
                    Role = x.Field<int>("FK_ROLE"),
                    TableGroupType = x.Field<int>("TABLEGROUPTYPE"),
                    Scope = x.Field<int>("SCOPE"),
                    Direct = x.Field<bool>("DIRECT"),
                    Read = x.Field<bool>("READ"),
                    Write = x.Field<bool>("WRITE"),
                    Insert = x.Field<bool>("INSERT"),
                    Delete = x.Field<bool>("DELETE"),
                    All = x.Field<bool>("ALL"),
                    PermissionGroup = x.Field<Guid?>("FK_PERMISSIONGROUP"),
                    TableInfo = x.Field<Guid?>("FK_TABLEINFO"),
                    ColumnInfo = x.Field<Guid?>("FK_COLUMNINFO"),
                    WorkflowEvent = x.Field<Guid?>("FK_WORKFLOWEVENT"),
                    SystemReport = x.Field<Guid?>("FK_SYSTEMREPORT"),
                    CustomReport = x.Field<Guid?>("FK_CUSTOMREPORT"),
                    UserReport = x.Field<Guid?>("FK_USERREPORT"),
                    SystemScript = x.Field<Guid?>("FK_SYSTEMSCRIPT"),
                    CustomScript = x.Field<Guid?>("FK_CUSTOMSCRIPT"),
                    UserScript = x.Field<Guid?>("FK_USERSCRIPT"),
                    HomeWidget = x.Field<Guid?>("FK_IQHOMEWIDGET").HasValue
                        ? widgets.First(y => y.WidgetId == x.Field<Guid>("FK_IQHOMEWIDGET")).Name
                        : "",
                    QlikApp = x.Field<string>("QLIKAPPID"),
                    Custom = x.Field<string>("CUSTOM"),
                    UIAction = x.Field<Guid?>("UIACTION"),
                    ActionId = x.Field<string>("ACTIONID"),
                    FormPart = x.Field<string>("FORMPARTID"),
                    FilterName = x.Field<string>("FILTERNAME"),
                    RecordFilter = x.Field<Byte[]>("RECORDFILTER"),
                }).Where(x =>
                    (!x.TableInfo.HasValue || (x.TableInfo.HasValue && !tablesToExclude.Contains(x.TableInfo.Value)))
                    && (!x.ColumnInfo.HasValue ||
                        (x.ColumnInfo.HasValue && !columnsToExclude.Contains(x.ColumnInfo.Value)))
                    && x.QlikApp == "").ToList();
        }

        public class Role
        {
            public int RoleId { get; set; }
            public string Name { get; set; }
        }

        class Widget
        {
            public Guid WidgetId { get; set; }
            public string Name { get; set; }
        }

        public class Layout
        {
            public int LayoutId { get; set; }
            public string LayoutName { get; set; }
            public string DesignTarget { get; set; }
            public string Name { get; set; }
            public string FormType { get; set; }
            public string Context { get; set; }
            public Role Role { get; set; }
            public Byte[] FormLayoutInfo { get; set; }
            public Byte[] FormSettingInfo { get; set; }

            public bool SameLayout(Layout l)
            {
                return l.LayoutName.Equals(LayoutName, StringComparison.OrdinalIgnoreCase) &&
                       l.DesignTarget == DesignTarget &&
                       l.Name.Equals(Name, StringComparison.OrdinalIgnoreCase) &&
                       l.FormType == FormType &&
                       l.Context == Context &&
                       ((l.Role == null && Role == null) ||
                        (l.Role != null && Role != null && l.Role.Name == Role.Name));
            }

            public bool LayoutHasSameFormLayoutInfo(Layout l)
            {
                if (l.FormLayoutInfo == FormLayoutInfo)
                {
                    return true;
                }

                if ((l.FormLayoutInfo != null) && (FormLayoutInfo != null))
                {
                    if (l.FormLayoutInfo.Length != FormLayoutInfo.Length)
                    {
                        return false;
                    }

                    for (int i = 0; i < l.FormLayoutInfo.Length; i++)
                    {
                        if (l.FormLayoutInfo[i] != FormLayoutInfo[i])
                        {
                            return false;
                        }
                    }

                    return true;
                }

                return false;
            }
        }

        public class Filter
        {
            public Guid FilterId { get; set; }
            public string SelectionName { get; set; }
            public DesignTarget DesignTarget { get; set; }
            public Guid Table { get; set; }
            public int? Role { get; set; }
            public byte[] Selection { get; set; }

            public bool SameFilter(Filter f)
            {
                return f.FilterId == FilterId;
            }

            public bool FilterHasSameSelection(Filter f)
            {
                if (f.Selection == Selection)
                {
                    return true;
                }

                if ((f.Selection != null) && (Selection != null))
                {
                    if (f.Selection.Length != Selection.Length)
                    {
                        return false;
                    }

                    for (int i = 0; i < f.Selection.Length; i++)
                    {
                        if (f.Selection[i] != Selection[i])
                        {
                            return false;
                        }
                    }

                    return true;
                }

                return false;
            }
        }

        public class RolePermission
        {
            public Guid RolePermissionId { get; set; }
            public int Role { get; set; }
            public int TableGroupType { get; set; }
            public int Scope { get; set; }
            public bool Direct { get; set; }
            public bool Read { get; set; }
            public bool Write { get; set; }
            public bool Insert { get; set; }
            public bool Delete { get; set; }
            public bool All { get; set; }
            public Guid? PermissionGroup { get; set; }
            public Guid? TableInfo { get; set; }
            public Guid? ColumnInfo { get; set; }
            public Guid? WorkflowEvent { get; set; }
            public Guid? SystemReport { get; set; }
            public Guid? CustomReport { get; set; }
            public Guid? UserReport { get; set; }
            public Guid? SystemScript { get; set; }
            public Guid? CustomScript { get; set; }
            public Guid? UserScript { get; set; }
            public string HomeWidget { get; set; }
            public string QlikApp { get; set; }
            public string Custom { get; set; }
            public Guid? UIAction { get; set; }
            public string ActionId { get; set; }
            public string FormPart { get; set; }
            public string FilterName { get; set; }
            public Byte[] RecordFilter { get; set; }

            public bool SameRolePermission(RolePermission obj)
            {
                return obj.TableGroupType == TableGroupType && obj.Scope == Scope &&
                       obj.PermissionGroup == PermissionGroup
                       && obj.TableInfo == TableInfo && obj.ColumnInfo == ColumnInfo &&
                       obj.WorkflowEvent == WorkflowEvent && obj.SystemReport == SystemReport
                       && obj.CustomReport == CustomReport && obj.UserReport == UserReport &&
                       obj.SystemScript == SystemScript && obj.CustomScript == CustomScript
                       && obj.UserScript == UserScript && obj.QlikApp == QlikApp && obj.Custom == Custom 
                       && obj.UIAction == UIAction && obj.HomeWidget == HomeWidget && obj.ActionId == ActionId 
                       && obj.FormPart == FormPart;
            }

            public bool RolePermissionHasSamePermissions(RolePermission obj)
            {
                //TableGroupType 6 = Filter
                return (obj.TableGroupType == 6)
                    ? obj.FilterName == FilterName && SameByteArray(obj.RecordFilter, RecordFilter)
                    : obj.Direct == Direct && obj.Read == Read && obj.Write == Write && obj.Insert == Insert &&
                      obj.Delete == Delete && obj.All == All;
            }
        }

        private static List<MailReportSetting> GetMailReportSettings(SdkSession mainSession)
        {
            return mainSession.GetRecordsetColumns("R_MAILREPORTSETTING", "", "")
                .DataTable.AsEnumerable().Select(x => new MailReportSetting()
                {
                    MailReportId = x.Field<int>("PK_R_MAILREPORTSETTING"),
                    TableInfo = x.Field<Guid>("FK_TABLEINFO"),
                    Default = x.Field<bool>("DEFAULT"),
                    Description = x.Field<string>("DESCRIPTION"),
                    FileName = x.Field<byte[]>("FILENAME"),
                    UserReport = x.Field<Guid?>("FK_USERREPORT"),
                    SystemReport = x.Field<Guid?>("FK_SYSTEMREPORT"),
                    From = x.Field<byte[]>("FROM"),
                    To = x.Field<byte[]>("TO"),
                    CC = x.Field<byte[]>("CC"),
                    BCC = x.Field<byte[]>("BCC"),
                    Subject = x.Field<byte[]>("SUBJECT"),
                    Salutation = x.Field<byte[]>("SALUTATION"),
                    HtmlBody = x.Field<string>("BODY"),
                    Script = x.Field<string>("SCRIPT"),
                    Translations =
                        (Dictionary<int, string>)mainSession.Sdk.GetTranslatableFieldValues("R_MAILREPORTSETTING",
                            "BODY", x.Field<int>("PK_R_MAILREPORTSETTING"))
                }).ToList();
        }

        public class MailReportSetting
        {
            public int MailReportId { get; set; }
            public Guid TableInfo { get; set; }
            public bool Default { get; set; }
            public string Description { get; set; }
            public byte[] FileName { get; set; }
            public Guid? UserReport { get; set; }
            public Guid? SystemReport { get; set; }
            public byte[] From { get; set; }
            public byte[] To { get; set; }
            public byte[] CC { get; set; }
            public byte[] BCC { get; set; }
            public byte[] Subject { get; set; }
            public byte[] Salutation { get; set; }
            public string HtmlBody { get; set; }
            public string Script { get; set; }
            public Dictionary<int, string> Translations { get; set; }

            public bool SameMailReportSetting(MailReportSetting mrs)
            {
                return mrs.TableInfo == TableInfo && mrs.Default == Default;
            }

            public bool PropertiesMailReportSettingAreEqual(MailReportSetting mrs)
            {
                return mrs.Description == Description && SameByteArray(mrs.FileName, FileName)
                                                      && mrs.UserReport == UserReport
                                                      && mrs.SystemReport == SystemReport &&
                                                      SameByteArray(mrs.From, From) &&
                                                      SameByteArray(mrs.To, To) && SameByteArray(mrs.CC, CC) &&
                                                      SameByteArray(mrs.BCC, BCC)
                                                      && SameByteArray(mrs.Subject, Subject) &&
                                                      SameByteArray(mrs.Salutation, Salutation)
                                                      && mrs.HtmlBody == HtmlBody && mrs.Script == Script &&
                                                      SameTranslations(mrs.Translations, Translations);
            }

            public bool SameTranslations(Dictionary<int, string> a, Dictionary<int, string> b)
            {
                if (a == b)
                {
                    return true;
                }

                if(a == null && b == null)
                {
                    return true;
                }

                if(a.Count == 0 && b.Count == 0)
                {
                    return true;
                }

                if ((a != null) && (b != null))
                {
                    if (a.Count != b.Count)
                    {
                        return false;
                    }

                    foreach (var keyValuePair in a)
                    {
                        if(!b.ContainsKey(keyValuePair.Key))
                        {
                            return false;
                        }

                        if(!b.TryGetValue(keyValuePair.Key, out string bValue))
                        {
                            return false;
                        }

                        if (bValue != keyValuePair.Value)
                        {
                            return false;
                        }
                    }

                    return true;
                }

                return false;
            }
        }

        public static bool SameByteArray(byte[] a, byte[] b)
        {
            if (a == b)
            {
                return true;
            }

            if ((a != null) && (b != null))
            {
                if (a.Length != b.Length)
                {
                    return false;
                }

                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i] != b[i])
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public enum DesignTarget : int
        {
            System = 1,
            Base = 2,
            Role = 3,
            User = 4
        }
    }
}
